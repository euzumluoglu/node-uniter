package emin.uzum.tests;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import emin.uzum.io.FileAccessorFactory;
import emin.uzum.io.IFileAccessor;

public class FileAccessorTest {

	private IFileAccessor fileAccessor;
	@Before
	public void setUp() throws Exception {
		fileAccessor = FileAccessorFactory.buildFileAccessor();
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void fileObjectNotNull(){
		Assert.assertNotNull(fileAccessor);
	}

	@Test
	public void fileNotFound() {
		try {
			fileAccessor.readFile("wrongFile");
			Assert.assertTrue("file not found",false);
		} catch (IOException e) {
			Assert.assertTrue("file found",true);
		}
	}
	
	@Test
	public void readFile() {
		try {
			List<String> contextList = fileAccessor.readFile("src/test/resources/input");
			Assert.assertNotNull(contextList);
			Assert.assertEquals(contextList.size(), 7);
		} catch (IOException e) {
		Assert.assertTrue(false);
		}
	}
	
	@Test
	public void writeFile() {
		String[] array = {"3","5","8"};
		List<String> fileConentList = Arrays.asList(array );
		fileAccessor.writeFile("src/test/resources/output", fileConentList );
		
		try {
			List<String> contextList = fileAccessor.readFile("src/test/resources/output");
			Assert.assertEquals(contextList.size(), 3);
		} catch (IOException e) {
			Assert.assertTrue(e.getMessage(),false);
		}
	}

}
