package emin.uzum.tests;


import org.junit.Assert;
import org.junit.Test;

import emin.uzum.nodes.NodeUniter;

public class NodeUniterTest {

	@Test
	public void checkRoundWithCorrectMap() {
		int round = NodeUniter.getUnited(8, "0 1 2 0 0 3 3");
		Assert.assertEquals(round, 4);
		round = NodeUniter.getUnited(9, "0 1 1 1 1 0 2 2");
		Assert.assertEquals(round, 5);
	}
	
	@Test
	public void checkRoundWithWrongMap() {
		int round = NodeUniter.getUnited(9, "0 1 2 0 0 3 3");
		Assert.assertEquals(round, -1);
	}

}
