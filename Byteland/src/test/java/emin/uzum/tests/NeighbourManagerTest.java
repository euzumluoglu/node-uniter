package emin.uzum.tests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import emin.uzum.graps.GraphFactory;
import emin.uzum.graps.IGraph;
import emin.uzum.relations.INeighbourManager;
import emin.uzum.relations.NeighbourManagerFactory;

public class NeighbourManagerTest {

	private INeighbourManager neighbourManager;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void checkRoundNumber() {
		IGraph graphRight = GraphFactory.buildGraph(8, "0 1 2 0 0 3 3");
		neighbourManager = NeighbourManagerFactory.buildNeighbourManager(graphRight.getNodeList());
		int roundNumber = neighbourManager.startUnited();
		Assert.assertEquals(roundNumber, 4);
		graphRight = GraphFactory.buildGraph(9, "0 1 1 1 1 0 2 2");
		neighbourManager = NeighbourManagerFactory.buildNeighbourManager(graphRight.getNodeList());
		roundNumber = neighbourManager.startUnited();
		Assert.assertEquals(roundNumber, 5);
	}
	
	@Test
	public void checkConnectionFailure() {
		IGraph graphWrong= GraphFactory.buildGraph(6, "0 4 4 5");
		neighbourManager = NeighbourManagerFactory.buildNeighbourManager(graphWrong.getNodeList());
		int roundNumber = neighbourManager.startUnited();
		Assert.assertEquals(roundNumber, -1);
	}

}
