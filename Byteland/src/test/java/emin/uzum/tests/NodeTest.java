package emin.uzum.tests;

import org.junit.Assert;
import org.junit.Test;

import emin.uzum.nodes.INode;
import emin.uzum.nodes.NodeFactory;

public class NodeTest {

	@Test
	public void checkNodeName() {
		String nodeName = "0";
		INode nodeOne = NodeFactory.buildNode(nodeName);
		Assert.assertEquals(nodeOne.getNodeName(), nodeName);
	}
	
	@Test
	public void connectDifferentNodes(){
		INode nodeOne = NodeFactory.buildNode("0");
		INode nodeTwo = NodeFactory.buildNode("1");
		Assert.assertTrue(nodeOne.connectNeighbours(nodeTwo));
	}
	
	@Test
	public void connectMuiltipleTimes(){
		INode nodeOne = NodeFactory.buildNode("0");
		INode nodeTwo = NodeFactory.buildNode("1");
		Assert.assertTrue(nodeOne.connectNeighbours(nodeTwo));
		Assert.assertFalse(nodeTwo.connectNeighbours(nodeOne));
		Assert.assertFalse(nodeOne.connectNeighbours(nodeTwo));
	}
	
	@Test
	public void discconectConnectedNodes(){
		INode nodeOne = NodeFactory.buildNode("0");
		INode nodeTwo = NodeFactory.buildNode("1");
		Assert.assertTrue(nodeOne.connectNeighbours(nodeTwo));
		Assert.assertTrue(nodeOne.disconnectNeighbours(nodeTwo));
	}
	
	@Test
	public void discconectNotConnectedNodes(){
		INode nodeOne = NodeFactory.buildNode("0");
		INode nodeTwo = NodeFactory.buildNode("1");
		Assert.assertFalse(nodeOne.disconnectNeighbours(nodeTwo));
	}
	
	@Test
	public void discconectMuiltipleTimesConnectedNodes(){
		INode nodeOne = NodeFactory.buildNode("0");
		INode nodeTwo = NodeFactory.buildNode("1");
		Assert.assertTrue(nodeOne.connectNeighbours(nodeTwo));
		Assert.assertTrue(nodeOne.disconnectNeighbours(nodeTwo));
		Assert.assertFalse(nodeOne.disconnectNeighbours(nodeTwo));
		Assert.assertFalse(nodeTwo.disconnectNeighbours(nodeOne));
	}
	
	@Test
	public void checkFreeNeighbours(){
		INode nodeOne = NodeFactory.buildNode("0");
		INode nodeTwo = NodeFactory.buildNode("1");
		Assert.assertEquals(nodeOne.getFreeNeighboursCount(), 0);
		Assert.assertEquals(nodeTwo.getFreeNeighboursCount(), 0);
		Assert.assertTrue(nodeOne.connectNeighbours(nodeTwo));
		Assert.assertEquals(nodeOne.getFreeNeighboursCount(), 0);
		Assert.assertEquals(nodeTwo.getFreeNeighboursCount(), 0);
		nodeOne.initializeFree();
		nodeTwo.initializeFree();
		Assert.assertEquals(nodeOne.getFreeNeighboursCount(), 1);
		Assert.assertEquals(nodeTwo.getFreeNeighboursCount(), 1);
	}
	
	@Test 
	public void checkIsBusy(){
		INode nodeOne = NodeFactory.buildNode("0");
		INode nodeTwo = NodeFactory.buildNode("1");
		Assert.assertFalse(nodeTwo.isBusy());
		Assert.assertFalse(nodeOne.isBusy());
		Assert.assertTrue(nodeOne.connectNeighbours(nodeTwo));
		Assert.assertFalse(nodeOne.meltNodes());
		Assert.assertFalse(nodeTwo.isBusy());
		Assert.assertFalse(nodeOne.isBusy());
		Assert.assertTrue(nodeOne.initializeFree());
		Assert.assertTrue(nodeTwo.initializeFree());
		Assert.assertTrue(nodeOne.meltNodes());
		Assert.assertTrue(nodeTwo.isBusy());
		Assert.assertTrue(nodeOne.isBusy());
	}
	
	@Test 
	public void clearBusy(){
		INode nodeOne = NodeFactory.buildNode("0");
		INode nodeTwo = NodeFactory.buildNode("1");
		Assert.assertTrue(nodeOne.connectNeighbours(nodeTwo));
		Assert.assertTrue(nodeOne.initializeFree());
		Assert.assertTrue(nodeTwo.initializeFree());
		Assert.assertTrue(nodeOne.meltNodes());
		Assert.assertTrue(nodeTwo.isBusy());
		Assert.assertTrue(nodeOne.isBusy());
		nodeTwo.clearBusy();
		Assert.assertFalse(nodeTwo.isBusy());
	}
	
	@Test 
	public void meltNode(){
		INode nodeOne = NodeFactory.buildNode("0");
		INode nodeTwo = NodeFactory.buildNode("1");
		Assert.assertTrue(nodeOne.connectNeighbours(nodeTwo));
		Assert.assertTrue(nodeOne.initializeFree());
		Assert.assertTrue(nodeTwo.initializeFree());
		Assert.assertTrue(nodeOne.meltNodes());
		Assert.assertEquals(nodeTwo.getNodeName(), "10");
	}
}
