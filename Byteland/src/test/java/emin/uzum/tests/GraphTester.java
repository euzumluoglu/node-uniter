package emin.uzum.tests;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import emin.uzum.graps.GraphFactory;
import emin.uzum.graps.IGraph;
import emin.uzum.nodes.INode;


public class GraphTester {
	
	@Test
	public void exactNodeCount(){
		IGraph graphRight = GraphFactory.buildGraph(8, "0 1 2 0 0 3 3");
		List<INode> nodeList = graphRight.getNodeList();
		Assert.assertNotNull(nodeList);
		Assert.assertEquals(8, nodeList.size());
	}
	
	@Test
	public void wrongNumber(){
		IGraph graphWrong = GraphFactory.buildGraph(9, "0 1 2 0 0 3 3");
		List<INode> nodeList = graphWrong.getNodeList();
		Assert.assertNull(nodeList);
	}
}
