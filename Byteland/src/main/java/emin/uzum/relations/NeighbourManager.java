package emin.uzum.relations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import emin.uzum.nodes.INode;

public class NeighbourManager implements INeighbourManager{

	private List<INode> nodeList;
	private List<INode> busyList;

	public NeighbourManager(List<INode> nodeList) {
		this.nodeList = nodeList;
		busyList = new ArrayList<INode>();

	}

	public int startUnited() {
		int round = 1;
		while (unitedRound()) {
			++round;
		}
		if(nodeList.size()>1){
			return -1;
		}
		return round;
	}

	private boolean unitedRound() {
		while (uniteSingleConnected());
		boolean isNoNeighbour = initializeNodeList();
		return isNoNeighbour;
	}

	private boolean uniteSingleConnected() {
		Collections.sort(nodeList);
		Iterator<INode> nodeIterator = nodeList.iterator();
		INode node;
		boolean isAnyUnited = false;
		while (nodeIterator.hasNext()) {
			node = nodeIterator.next();

			if (node.isBusy() || !node.isFreeNeighboursExist()) {
				busyList.add(node);
				nodeIterator.remove();
				continue;
			} 

			if (node.meltNodes()) {
				isAnyUnited = true;
				nodeIterator.remove();
			}
		}
		return isAnyUnited;
	}

	private boolean initializeNodeList() {
		boolean isNoNeighbour = false;
		for (INode node : busyList) {
			if(node.initializeFree()){
				isNoNeighbour = true;
			}
			nodeList.add(node);
		}
		busyList.clear();
		return isNoNeighbour;
	}

}
