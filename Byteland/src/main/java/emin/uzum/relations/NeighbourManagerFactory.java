package emin.uzum.relations;

import java.util.List;

import emin.uzum.nodes.INode;

public class NeighbourManagerFactory {

	public static INeighbourManager buildNeighbourManager(List<INode> nodeList){
		return new NeighbourManager(nodeList);
	}
}
