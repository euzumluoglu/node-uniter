
package emin.uzum.graps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import emin.uzum.nodes.INode;
import emin.uzum.nodes.Node;
import emin.uzum.nodes.NodeFactory;

public class Graph implements IGraph{

	private Map<String, INode> nodeMap;
	private int nodeCount;

	public Graph(int nodeCount, String relations) {
		this.nodeCount = nodeCount;
		nodeMap = new HashMap<String, INode>(nodeCount);
		initialize(relations);
	}

	public List<INode> getNodeList() {
		ArrayList<INode> nodeList = new ArrayList<INode>(nodeMap.values());
		if(nodeCount==nodeList.size()){
			for(INode node:nodeList){
				node.initializeFree();
			}
			return nodeList;
		}
		return null;
	}

	private void initialize(String relations) {
		String[] relationList = relations.split(" ");
		String neighbourName;
		for (int i = 0; i < relationList.length; i++) {
			neighbourName = "" + (i + 1);
			connectNodes(relationList[i], neighbourName);
		}
	}


	private void connectNodes(String nodeOne, String nodeTwo) {
		INode firstNode =  findNodeFromMap(nodeOne);
	    INode secondNode = findNodeFromMap(nodeTwo);
	    firstNode.connectNeighbours(secondNode);
	}

	private Node findNodeFromMap(String nodeName) {
		INode node = nodeMap.get(nodeName);
		if (node == null) {
			node = NodeFactory.buildNode(nodeName);
			nodeMap.put(nodeName, node);
		}
		return (Node) node;
	}
}
