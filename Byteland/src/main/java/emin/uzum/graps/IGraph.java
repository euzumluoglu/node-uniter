package emin.uzum.graps;

import java.util.List;

import emin.uzum.nodes.INode;

public interface IGraph {

	public List<INode> getNodeList();
}
