package emin.uzum.io;

import java.io.IOException;
import java.util.List;

public interface IFileAccessor{

	public List<String> readFile(String filePath) throws IOException;
	
	public void writeFile(String filePath,List<String> fileContextList);
}
