package emin.uzum.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileAccessor implements IFileAccessor{

	public List<String> readFile(String filePath) throws IOException {
		BufferedReader br = null;
		List<String> fileConentList = new ArrayList<String>();
		try {

			String currentLine;

			br = new BufferedReader(new FileReader(filePath));

			while ((currentLine = br.readLine()) != null) {
				fileConentList.add(currentLine);
			}

		} 

		finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return fileConentList;
	}

	public void writeFile(String filePath,List<String> fileContextList) {
		try {

			if(fileContextList==null || fileContextList.isEmpty()){
				return;
			}
			File file = new File(filePath);
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			for(String content:fileContextList){
				bw.write(content);
				bw.newLine();
			}
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
