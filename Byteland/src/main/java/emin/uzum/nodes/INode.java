package emin.uzum.nodes;

public interface INode extends Comparable<INode> {

	public boolean initializeFree();

	public int getFreeNeighboursCount();

	public boolean connectNeighbours(INode neighbour);

	public boolean disconnectNeighbours(INode neighbour);

	public String getNodeName();

	public boolean isBusy();

	public boolean meltNodes();

	public void clearBusy();

	public boolean isFreeNeighboursExist();

}
