package emin.uzum.nodes;

import java.util.ArrayList;
import java.util.List;

public class Node implements INode {

	private String nodeName;
	private boolean isBusy = false;

	private List<INode> neighbours;
	private List<INode> freeNeighbours;

	public Node(String nodeName) {
		this.nodeName = nodeName;
		neighbours = new ArrayList<INode>();
		freeNeighbours = new ArrayList<INode>();
	}

	private void hearNeighbourBusy(INode node) {
		freeNeighbours.remove(node);
	}

	public void publishIBusy() {
		for (INode neighbour : freeNeighbours) {
			((Node) neighbour).hearNeighbourBusy(this);
		}
	}

	public boolean initializeFree() {
		if (neighbours.isEmpty()) {
			return false;
		}
		freeNeighbours.clear();
		freeNeighbours.addAll(neighbours);
		isBusy = false;
		return true;
	}

	public int getFreeNeighboursCount() {
		return freeNeighbours.size();
	}

	public INode getConnectableNeighbour() {
		return freeNeighbours.get(0);
	}

	public boolean meltNodes() {
		if (this.freeNeighbours.size() != 1 || this.isBusy) {
			return false;
		}
		Node neighbour = (Node) this.freeNeighbours.get(0);
		if (neighbour.isBusy() || !neighbour.meltNodes(this)) {
			return false;
		}
		this.isBusy = true;
		neighbour.isBusy = true;
		return true;
	}

	public boolean meltNodes(INode neighbour) {
		if (!this.neighbours.contains(neighbour)) {
			return false;
		}
		this.disconnectNeighbours(neighbour);
		this.publishIBusy();
		((Node) neighbour).publishIBusy();
		this.nodeName += neighbour.getNodeName();
		List<INode> newNeighbours = ((Node) neighbour).changeMeWithNew(this);
		return true;
	}

	public String getNodeName() {
		return nodeName;
	}

	// public void setNodeName(String nodeName) {
	// this.nodeName = nodeName;
	// }

	public int compareTo(INode node) {
		return getFreeNeighboursCount() - node.getFreeNeighboursCount();
	}

	public boolean isBusy() {
		return isBusy;
	}

	public void clearBusy() {
		isBusy = false;
	}

	public boolean isFreeNeighboursExist() {
		if (freeNeighbours.isEmpty() || freeNeighbours.size() == 0) {
			return false;
		}
		return true;
	}

	private boolean addNeighbour(INode neighbour) {
		return this.neighbours.add(neighbour);
	}

	private boolean removeNeighbour(INode neighbour) {
		return this.neighbours.remove(neighbour);
	}

	public boolean connectNeighbours(INode neighbour) {
		if(this.neighbours.contains(neighbour)){
			return false;
		}
		if(((Node) neighbour).neighbours.contains(this)){
			return false;
		}
		this.addNeighbour(neighbour);
		((Node) neighbour).addNeighbour(this);
		return true;
	}

	public boolean disconnectNeighbours(INode neighbour) {
		boolean isAddOne = this.removeNeighbour(neighbour);
		boolean isAddedTwo = ((Node) neighbour).removeNeighbour(this);
		return (isAddOne && isAddedTwo);
	}

	private List<INode> changeMeWithNew(INode newNeighbour) {
		List<INode> neighbourList = this.neighbours;
		for (INode neighbour : neighbourList) {
			((Node) neighbour).removeNeighbour(this);
			newNeighbour.connectNeighbours(neighbour);
		}
		return neighbourList;
	}

}
