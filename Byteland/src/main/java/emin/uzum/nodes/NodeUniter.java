package emin.uzum.nodes;

import java.util.List;

import emin.uzum.graps.GraphFactory;
import emin.uzum.graps.IGraph;
import emin.uzum.relations.INeighbourManager;
import emin.uzum.relations.NeighbourManagerFactory;

public class NodeUniter {
	
	public static int getUnited(int nodeCount, String relations) {
		IGraph graph = GraphFactory.buildGraph(nodeCount, relations);
		List<INode> nodeList = graph.getNodeList();
		if(nodeList==null || nodeList.isEmpty()){
			return -1;
		}
		INeighbourManager neighbourManager = NeighbourManagerFactory.buildNeighbourManager(nodeList);
		int rountNumber = neighbourManager.startUnited();
		return rountNumber;
	}
	
}
