package emin.uzum.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import emin.uzum.io.FileAccessorFactory;
import emin.uzum.io.IFileAccessor;
import emin.uzum.nodes.NodeUniter;

public class GraphAnalyzer {

	public static void main(String[] args) {
		if (args == null || args.length < 2) {
			System.out.println("parameters is not enough");
			return;
		}
		init(args[0], args[1]);
		System.out.println("see you");
	}
	
	private static void init(String inputFilePath, String outputFilePath){
		List<String> inputContextList = readInputFile(inputFilePath);
		List<String> outputContextList = initialize(inputContextList);
		writeOutputFile(outputFilePath, outputContextList);
	}
	
	private static void writeOutputFile(String outputFile, List<String> contextList) {
		IFileAccessor fileAccessor = FileAccessorFactory.buildFileAccessor();
		fileAccessor.writeFile(outputFile, contextList);
	}
	
	private static List<String> readInputFile(String inputFile) {
		IFileAccessor fileAccessor = FileAccessorFactory.buildFileAccessor();
		List<String> contextList = null;
		try {
			contextList = fileAccessor.readFile(inputFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return contextList;
	}
	
	private static List<String> initialize(List<String> inputContextList) {
		List<String> outputContextList;
		if (inputContextList == null || inputContextList.isEmpty()) {
			outputContextList = new ArrayList<String>(1);
			outputContextList.add("Empty input list");
			return outputContextList;
		}
		String numberOfProcessString = inputContextList.get(0);
		int numberOfProcess = Integer.parseInt(numberOfProcessString);
		int numberOfLine = numberOfProcess *2;
		if (inputContextList.size() != (numberOfLine + 1)) {
			outputContextList = new ArrayList<String>(1);
			outputContextList.add("numberOfProcess not meet the list");
			return outputContextList;
		}
		outputContextList = getRoundNumbers(inputContextList, numberOfProcess, numberOfLine);
		return outputContextList;
	}

	private static List<String> getRoundNumbers(List<String> inputContextList, int numberOfProcess, int numberOfLine) {
		List<String> outputContextList = new ArrayList<String>(numberOfProcess);
		int nodeCount;
		String relations;
		String message;
		for(int i=1; i<=numberOfLine; i+=2){
			nodeCount = Integer.parseInt(inputContextList.get(i));
			if(nodeCount<2 || nodeCount>600){
				message = "node count ["+nodeCount+ "] is not suitable (must be between 600>= && >=2)";
				outputContextList.add(message);
				continue;
			}
			relations = inputContextList.get(i+1);
			message = getRound(nodeCount, relations);
			outputContextList.add("" + message);
		}
		return outputContextList;
	}
	
	private static String getRound(int nodeCount, String relations){
		int roundNumber = NodeUniter.getUnited(nodeCount, relations);
		if (roundNumber == -1){
			return "There is a node relation problem";
		}
		return ""+roundNumber;
	}

}
